package com.examalbo.alboexamenpart1.data.raw

import com.examalbo.alboexamenpart1.common.ReadRaw
import com.examalbo.alboexamenpart1.data.model.Expenses

class Raw {
    private  val readRaw: ReadRaw

    fun readJson():Expenses{
       return readRaw.read()
    }

    init {
        readRaw = ReadRaw()

    }
}