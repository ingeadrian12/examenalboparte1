package com.examalbo.alboexamenpart1.data.model

data class ExpensesItem(
    val amount: Double,
    val category: String,
    val creation_date: String,
    val description: String,
    val operation: String,
    val status: String,
    val uuid: Int
)