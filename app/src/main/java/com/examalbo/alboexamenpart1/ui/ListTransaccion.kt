package com.examalbo.alboexamenpart1.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.examalbo.alboexamenpart1.R
import com.examalbo.alboexamenpart1.data.model.Expenses
import com.examalbo.alboexamenpart1.data.raw.Raw
import com.examalbo.alboexamenpart1.ui.modelview.BlankViewModel

class ListTransaccion : Fragment() {

    private lateinit var expenses: Expenses
    private lateinit var raw: Raw
    companion object {
        fun newInstance() = ListTransaccion()
    }

    private lateinit var viewModel: BlankViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.blank_fragment, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BlankViewModel::class.java)
        init()
    }
    private fun init(){
      //  viewModel.listBeers.observe(viewLifecycleOwner, beersObserver)
        raw = Raw()
        expenses = raw.readJson()

    }
}