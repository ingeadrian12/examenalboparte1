package com.examalbo.alboexamenpart1.common

import androidx.annotation.RawRes
import com.examalbo.alboexamenpart1.MyApp
import com.examalbo.alboexamenpart1.R
import com.examalbo.alboexamenpart1.data.model.Expenses
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ReadRaw {
    private  val gson: Gson = Gson()

    private inline fun <reified T> readRawJson(@RawRes rawResId: Int): T {
        MyApp.instance.resources.openRawResource(rawResId).bufferedReader().use {
            return gson.fromJson<T>(it, object: TypeToken<T>() {}.type)
        }
    }
    fun read():Expenses{
        return readRawJson(R.raw.array)
    }
}